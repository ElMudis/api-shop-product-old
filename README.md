API REST base FOSREST-User-OAuth-Skeleton
==========

Complete Symfony3 REST Skeleton w/ FOSRESTBundle, FOSUserBundle, FOSOAuthBundle Pre-Installed.

Installation:
================
```
git clone https://bitbucket.org/ElMudis/api-shop-product.git
cd api-shop-product
composer install
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:schema:update --force
```

Creates 5 new test products
======================
```    
php bin/console api:products:test:create
```

Create An OAuth Client
======================
Only use the grant-type you intend to use.

```    
php bin/console api:oauth-server:client:create --redirect-uri="http://127.0.0.1:8000/" --grant-type="authorization_code" --grant-type="password" --grant-type="refresh_token" --grant-type="token" --grant-type="client_credentials"
```

Create A User
=============
```
curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"user":{"username": "zac", "password": "1qaz", "email": "test@test.com"}}' http://127.0.0.1:8000/app_dev.php/users
```

### Get User Access & Refresh Tokens
Browse to the following URL while Replacing **CLIENTID**, **CLIENTSECRET**, **USERNAME**, & **PASSWORD** with your values:

```
http://127.0.0.1:8000/app_dev.php/oauth/v2/token?client_id=CLIENTID&client_secret=CLIENTSECRET&grant_type=password&username=USERNAME&password=PASSWORD
```

### See Authorization Failure:
The following will return a access_denied error:
```
curl -X GET -H "Content-Type: application/json" -H "Accept: application/json" http://127.0.0.1:8000/app_dev.php/api/products?access_token=ACCESSTOKEN
```


API Documentation
====
http://127.0.0.1:8000/doc