### PASSO A PASSO PARA ASSOCIAR A API AO PROJETO QCOMMERCE.

---

### Passo 01 &raquo; Remoção do ENUM das tabelas e do schema.xml

Abra o arquivo `/quality/qcommerce/propel/schema.xml` e sobrescreva `sqlType="ENUM` por `type="VARCHAR" size="30" description="`
> *Note que alguns campos ficarão com dois atributos descriptions, então devemos juntar ambos em apenas um.*  

Em seguida, execute o comando propel-gen para aplicar as alterações nas entidades.
```console
$ cd meuprojeto-qcommerce/quality
$ ./vendor/bin/propel-gen ./qcommerce/propel om
```

#### Ajustes para o Q.Commerce

#### &raquo; Categorias

Dois campos tiveram que ser criados nas categorias para um bom funcionamento do *behaviour* *Nested Set*.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<table name="categoria" phpName="Categoria">
    [...]
    <column name="root_id" type="INTEGER" size="10" />
    <column name="parent_id" type="INTEGER" size="10" />
    [...]
</table>
```

Execute o comando `$ ./vendor/bin/propel-gen ./qcommerce/propel om`

Adicione a inserção destes campos na ação post das categorias no admin.

```php
// quality/qcommerce/admin/categorias/actions/registration/action.php
# [...]
try
    {
        $parent = $_classPeer::retrieveByPK($request->request->get('PARENT_ID'));
        $data   = trata_post_array($request->request->get('data'));
        $object->setByArray($data);
        $object->setParentId($parent->getId()); # <---------
        $object->setRootId($root->getId());     # <---------

        if ($object->isNew())
        {
            $object->insertAsLastChildOf($parent);
        }
# [...]
```

#### Opcional &raquo; Migrations

Se você quiser adicionar migrations ao projeto para visualizar as mudanças de estruturas da base de dados e aplicá-las mais facilmente, siga os passos abaixo:
- Crie um arquivo chamado `buildtime-conf.xml` na pasta `/quality/qcommerce/propel` com o mesmo conteúdo do arquivo `runtime-conf.xml` encontrado na mesma pasta.
- Abra seu terminal (cmd, gitbash, ...) e execute os comandos abaixo dentro da pasta `/quality`:
```console
$ ./vendor/bin/propel-gen ./qcommerce/propel diff
```
Este comando irá gerar uma nova tabela no banco de dados configurado chamada migrations que possuirá a versão da última atualização executada.  

Mas o que realmente é interessante é que também será gerado uma classe PHP contendo todas as alterações da última versão que foi gerado o diff.  

Este arquivo ficará dentro de `/quality/qcommerce/propel/build/migrations`.

Ex.:
```php
<?php
/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1489586815.
 * Generated on 2017-03-15 15:06:55
 */
class PropelMigration_1489586815
{

    public function getUpSQL()
    {
        return array (
              'qcommerce' => '
                    SET FOREIGN_KEY_CHECKS = 0;
                    ALTER TABLE qp1_categoria
                        ADD root_id INT DEFAULT NULL,
                        ADD parent_id INT DEFAULT NULL;
                    ALTER TABLE qp1_categoria
                        ADD CONSTRAINT FK_45BE2DED727ACA70
                        FOREIGN KEY (parent_id) REFERENCES qp1_categoria (ID) ON DELETE CASCADE;
                    ALTER TABLE qp1_categoria
                    ADD CONSTRAINT FK_45BE2DED79066886
                        FOREIGN KEY (root_id) REFERENCES qp1_categoria (ID) ON DELETE CASCADE;
                    CREATE INDEX IDX_45BE2DED727ACA70
                        ON qp1_categoria (parent_id);
                    CREATE INDEX IDX_45BE2DED79066886
                        ON qp1_categoria (root_id);
                    SET FOREIGN_KEY_CHECKS = 1;
            ',
        );
    }

	public function getDownSQL()
	{
        return array (
              'qcommerce' => '
                    SET FOREIGN_KEY_CHECKS = 0;
                    ALTER TABLE qp1_categoria
                        DROP COLUMN root_id,
                        DROP COLUMN parent_id;
                    ALTER TABLE qp1_categoria
                        DROP INDEX IDX_45BE2DED79066886,
                        DROP INDEX IDX_45BE2DED727ACA70;
                    SET FOREIGN_KEY_CHECKS = 1;
            ',
        );
	}
}
```

Você pode executar o comando de adição no banco de dados.

Não aconcelho executar o comando `migrate` no qual aplica as alterações automaticamente para não excluir alguma tabela da API que tenha sido criada e que não
esteja mapeada `schema.xml` a não ser que você decida adicionar as tabelas relacionadas a autenticação da API (que serão vistas posteriormente) também no schema.xml, 
você poderá utilizar o propel-migrations sem problemas.

---

### Passo 02 &raquo; API

Caso não tenha o projeto, faça um fork do repoistório `padrao-qcommerce-api` para `meu-qcommerce-api`.
Faça o clone para seu workspace.

Instale as dependências:
```console
$ cd meuprojeto-qcommerce-api
$ bin/console composer install
```
Adicione as configurações do banco de dados do q.commerce que será associado à API.
> **Não se preocupe com os dados de SMTP.**

Gere o arquivo de sql da API para ser incluída na plataforma através do comando:
```console
bin/console dotrine:schema:update --dump-sql >> ./app/config/Resources/update-in-qcommerce.sql
```

**Mantenha apenas as criações, não exclua nada da base de dados atual, apenas adicione.**

O arquivo gerado deve ficar algo semelhante à:
```sql
CREATE TABLE api_access_token ...
CREATE TABLE api_auth_code ...
CREATE TABLE api_client ...
CREATE TABLE api_refresh_token ...
CREATE TABLE api_user ...
```

Execute este SQL manualmente na base de dados do q.commerce.

---

### Passo 03 &raquo; Configuração

1. Gere um registro na tabela `client`, para definir a aplicação que acessará a API, através do comando:
```console
$ bin/console api:oauth-server:client:create --redirect-uri="http://projeto.qcommerce.com.br" --grant-type="authorization_code" --grant-type="password" --grant-type="refresh_token" --grant-type="token" --grant-type="client_credentials"
```

A saída deste comando será algo parecido com:
> *Added a new client with public id 2_52dr9ggzzb8ko44s4o0csocoo84cc4k4kw0ck8w8oowg0c0gg4, secret 2zqihg8zuvuo4kcg0c0gocowkk8ogcsw4o8k88w4wsc8owgggk*

Estas *keys* `public_id` e `secret` deverão ser disponibilizadas a aplicação que acessará a API

2. Gere um usuário de acesso através do comando:
```console
$ curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d '{"user":{"username": "app_name", "password": "app_secret", "email": "test@test.com"}}' http://dominio/.../users
```

Em `app_name` você pode informar o nome da aplicação que acessará a API em conjunto com o `password`.

Tanto as informações de user, quanto as informações de client deverão ser informadas para quem consumir a api, pois essas informações são essenciais para
a geração do parâmetro `access_token` que é obrigatório em todas as requisições.

Exemplo do retorno:
```json
{"id":1,"username":"app_name","username_canonical":"app_secret","email":"test@test.com","email_canonical":"test@test.com","enabled":true,"password":"$2y$13$Qpna4\/n1L2yMyDXaUr3owO3DvxN4s.X.HMYiigxBb28V2VUmK1gDK","roles":[]}
```

Tanto os dados do client, quanto os dados do user são necessários na url de autenticação em busca do `access_token`.

---

### Pontos de atenção

* Se o e-commerce já posusir categorias cadastradas, o SQL abaixo associará as chaves `root_id` e `parent_id` aos registros existentes:

*No {id} deve conter o id da categoria root no projeto. Normalmente, é 1.*
```sql
UPDATE qp1_categoria SET root_id = {id};
```

*Atualiza o campo parent_id das categorias existentes.*
```sql
UPDATE qp1_categoria
LEFT JOIN qp1_categoria parent
	ON parent.NR_LFT < qp1_categoria.NR_RGT
		AND parent.NR_RGT > qp1_categoria.NR_RGT
		AND parent.NR_LVL = (qp1_categoria.NR_LVL - 1)
SET qp1_categoria.parent_id = parent.ID;
```

---

### Documentação de como consumir a API

O padrão de autenticação utilizado é através do OAuth2.
Sendo assim, para acessar qualquer módulo é necessário obter o access_token para enviá-lo por parâmetro nas requisições.

Este access_token é obtido da seguinte forma (lembrando que o caminho pode variar conforme for a publicação):

```console
$ curl -X GET -H "Content-Type: application/json" -H "Accept: application/json" "http://qcommerce.com.br/ambiente_testes/web/v1/web/app_dev.php/oauth/v2/token?grant_type=password&client_id=2_52dr9ggzzb8ko44s4o0csocoo84cc4k4kw0ck8w8oowg0c0gg4&client_secret=2zqihg8zuvuo4kcg0c0gocowkk8ogcsw4o8k88w4wsc8owgggk&username=app_name&password=app_secret"
```

> Note que as informações `client_id`, `client_secret`, `username` e `password` são informadas na busca do token.

Exemplo de retorno:
```json
{
    "access_token": "MDRiY2FjMTMzMmI4ZjVkMDg4NjMwYTY4N2VmNmE3MDczNmM0YWY5NGNiY2Y0YzkxZGM0N2NiNTQzMWE0M2Y2Yw",
    "expires_in": 3600,
    "token_type": "bearer",
    "scope": "user",
    "refresh_token": "OGQ3NjU4MTgyYzg3NWQ5OWI5MmI4ZDcwNTg0NjRkZjMxMmRhNTk4NjE0MjhjMTg0ZDA2NDk5YzMyNDIyMWQzMA"
}
```

Tendo o access_token em mãos, através das informações que estão na documentação, será possível acessar algumas informações do e-commerce.
Atualmente será possível:
- Cadastrar, editar, remover, listar e visualisar categorias e marcas;
- Listar e visualizar clientes e endereços;

##### Exemplo de acesso à listagem de categorias:

```console
$ curl -X GET -H "Accept: application/json" "http://lojazeusdobrasil.qcommerce.com.br/ambiente_testes/web/v1/web/app_dev.php/api/categorias?access_token=MDRiY2FjMTMzMmI4ZjVkMDg4NjMwYTY4N2VmNmE3MDczNmM0YWY5NGNiY2Y0YzkxZGM0N2NiNTQzMWE0M2Y2Yw"
```

##### Exemplo de inserção de uma categoria:

```console
curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d '{"nome": "Categoria teste inserida pela API", "parent": 3}' "http://lojazeusdobrasil.qcommerce.com.br/ambiente_testes/web/v1/web/app_dev.php/api/categorias?access_token=MDRiY2FjMTMzMmI4ZjVkMDg4NjMwYTY4N2VmNmE3MDczNmM0YWY5NGNiY2Y0YzkxZGM0N2NiNTQzMWE0M2Y2Yw"
```