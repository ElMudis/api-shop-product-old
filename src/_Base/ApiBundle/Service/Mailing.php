<?php

namespace _Base\ApiBundle\Service;


use _Base\ApiBundle\Entity\Pedido;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment as Environment;

class Mailing
{

    private $em;
    private $twig;
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->twig = $container->get('twig');
        $this->em = $container->get('doctrine')->getManager();
    }

    /**
     * @param $basePath String
     * @return String
     */
    public function pedidoCancelado(Pedido $pedido)
    {
        $body = $this->twig->render(
            'ApiBundle:mail_templates:pedido.cancelado.html.twig',
            array(
                'pedido' => $pedido
            )
        );

        $message = \Swift_Message::newInstance()
            ->setSubject('Pedido Cancelado')
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo($pedido->getCliente()->getEmail())
            ->setBody($body, 'text/html');


        $this->container->get('mailer')->send($message);
        return true;
    }

}