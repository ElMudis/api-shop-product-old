<?php
namespace _Base\BackendBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemCartType extends AbstractType
{

	/**
	 *
	 *
	 * @param FormBuilderInterface $builder
	 * @param array   $options
	 */
	public function buildForm( FormBuilderInterface $builder, array $options) {
		$builder
		->add('title', TextType::class, [
			'description' => 'Title'
			])

		->add('price', TextType::class, [
			'description' => 'Price'
			])



		->add('quantity', TextType::class, [
			'description' => 'Quantity'
			])

		;

	}

	/**
	 *
	 *
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(array(
				'data_class' => \_Base\BackendBundle\Entity\ItemCart::class,
				'csrf_protection' => false,
			));
	}
}
