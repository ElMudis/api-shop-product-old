<?php

namespace _Base\BackendBundle\Entity\Base;



use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Hateoas\Configuration\Annotation as Hateoas;


use _Base\BackendBundle\Entity\Cart;
use _Base\BackendBundle\Entity\Product;



/**
 *
 *
 * @ORM\MappedSuperclass
 */
class BaseItemCart extends BaseEntity
{
	/**
	 *
	 *
	 * @Serializer\Exclude()
	 */
	protected $SERVER_PATH_TO_IMAGE_FOLDER = "uploads/item_cart";

	/**
	 *
	 *
	 * @var int
	 *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
	 */
	protected $id;


	/**
	 * Title
	 *
	 * @var string
	 *
	 * @ORM\Column(type="string",  nullable = false )
	 * @Assert\NotBlank(message = "Title e obrigatorio")    */
	protected $title;
	/**
	 * Price
	 *
	 * @var float
	 *
	 * @ORM\Column(type="float",  nullable = false )
	 * @Assert\NotBlank(message = "Price e obrigatorio")*/
	protected $price;

	/**
	 * Many ItemCarts have one Cart.
	 *
	 * @ORM\ManyToOne(targetEntity="Cart", inversedBy="item_cart")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $cart;

	/**
	 * Many ItemCarts have one Product.
	 *
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="item_cart")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $product;

	/**
	 * Quantity
	 *
	 * @var int
	 *
	 * @ORM\Column(type="integer",  nullable = false )
	 * @Assert\NotBlank(message = "Quantity e obrigatorio")*/
	protected $quantity;
	public static function getCamposObrigatorios() {
		return array( 'title', 'price', 'quantity');
	}
	public static function getCampos() {
		return array('title', 'price', 'cart', 'product', 'quantity');
	}

	public static function getFrontendObjetoNome() {
		return "item_cart";
	}

	public function getCamposTraducoes() {
		return array(
			'title',
			'price',
			'quantity',
		);
	}

	/**
	 *
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->getTitle();
	}





	/**
	 * Constructor
	 */
	public function __construct() {
	}













	/**
	 * Set cart
	 *
	 * @param \_Base\BackendBundle\Entity\Cart $cart
	 *
	 * @return ItemCart
	 */
	public function setCart($cart) {
		if (!is_object($cart) && !is_null($cart) ){
			$em = $this->em;
			$cart = $em->getRepository('BackendBundle:Cart')->find($cart);
		}
		$this->cart = $cart;
		return $this;
	}

	/**
	 * Get cart
	 *
	 * @return \_Base\BackendBundle\Entity\Cart
	 */
	public function getCart()
	{
		return $this->cart;
	}



	public static function getListaCart(\Symfony\Component\DependencyInjection\Container $container)
	{
		$em = $container->get('doctrine')->getManager();

		$dql = "SELECT a.id as id, a1.texto as titulo FROM BackendBundle:Cart a LEFT JOIN BackendBundle:Traducao a1 WITH a.totalPrice = a1.codigo";
		$equiposTestes = $em->createQuery($dql)->execute();

		$listaEquipeTest = array();
		foreach ($equiposTestes as $equipo){
			$listaEquipeTest[$equipo['id']] = $equipo['titulo'];
		}

		return $listaEquipeTest;

	}



	/**
	 * Set product
	 *
	 * @param \_Base\BackendBundle\Entity\Product $product
	 *
	 * @return ItemCart
	 */
	public function setProduct($product)
	{
		if(!is_object($product) && !is_null($product) ){
			$em = $this->em;
			$product = $em->getRepository('BackendBundle:Product')->find($product);
		}
		$this->product = $product;
		return $this;
	}

	/**
	 * Get product
	 *
	 * @return \_Base\BackendBundle\Entity\Product
	 */
	public function getProduct()
	{
		return $this->product;
	}




	public static function getListaProduct(\Symfony\Component\DependencyInjection\Container $container)
	{
		$em = $container->get('doctrine')->getManager();
		$dql = "SELECT a.id as id, a1.texto as titulo FROM BackendBundle:Product a LEFT JOIN BackendBundle:Traducao a1 WITH a.title = a1.codigo";
		$equiposTestes = $em->createQuery($dql)->execute();

		$listaEquipeTest = array();
		foreach ($equiposTestes as $equipo){
			$listaEquipeTest[$equipo['id']] = $equipo['titulo'];
		}

		return $listaEquipeTest;

	}







}
