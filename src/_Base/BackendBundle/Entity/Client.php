<?php


namespace _Base\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Hateoas\Configuration\Annotation as Hateoas;

use _Base\BackendBundle\Entity\Base\BaseClient;




/**
 *
 *
 * @ORM\Entity(repositoryClass="_Base\BackendBundle\Repository\ClientRepository")
 * @ORM\Table(name="back_client")
 * @Hateoas\Relation(
 *     "self",
 *     href = @Hateoas\Route("get_client", parameters = {"id" = "expr(object.getId())"})
 * )
 */
class Client extends BaseClient
{





}
