<?php





namespace _Base\BackendBundle\Controller\Entidades;

use _Base\BackendBundle\Entity\Client;
use _Base\BackendBundle\Form\ClientType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use Hateoas\Configuration\Route;
use Proxies\__CG__\_Base\BackendBundle\Entity\Cart;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Response;
use _Base\ApiBundle\Controller\ApiController;


class ClientController extends ApiController
{
	/**
	 * Clients list
	 *
	 * @ApiDoc(resource=true)
	 *
	 * @QueryParam(name="page", description="Número da página. Padrão 1")
	 * @QueryParam(name="limit", description="Número de registros por página. Padrão 50")
	 * @QueryParam(name="name", description="Filtre pelo nome de alguma client")
	 *
	 * @param ParamFetcherInterface $paramFetcher
	 *
	 * @return Response
	 */
	public function cgetAction( ParamFetcherInterface $paramFetcher) {
		$queryBuilder = $this
		->getRepository(Client::class)
		->findByParamFetcher($paramFetcher, 'client');
		return $this->createResponseForCollection($paramFetcher, $queryBuilder, new Route('get_clients'));
	}

	/**
	 * Get a client
	 *
	 * @ApiDoc
	 *
	 * @param integer $id ID do registro
	 * @return Response
	 */
	public function getAction($id) {
		return $this->createResponseForFindOne(Client::class, $id);
	}

	/**
	 * Get client cart. If don't have create one
	 *
	 * @ApiDoc
	 *
	 * @param integer $id ID do registro
	 * @return Response
	 */
	public function getCartAction($id) {
		$em = $this->getDoctrine()->getManager();
		$client = $em->find('BackendBundle:Client',$id);

        if (!$client) {
            return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
        }


        $cart = $client->getCart();

		if(is_null($cart)){
		    $cart = new Cart();
            $cart->setManager($em);
		    $cart->setClient($client);
		    $cart->setTotalPrice(0);
            $client->setCart($cart);
		    $em->persist($cart);
		    $em->persist($client);
		    $em->flush();

            return $this->handleView($this->view($cart, Codes::HTTP_CREATED));
        }
        return $this->handleView($this->view($cart, Codes::HTTP_OK));
	}

	/**
	 * Add new client
	 *
	 * @ApiDoc(input={"class"="_Base\BackendBundle\Form\ClientType", "name"=""})
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function postAction(Request $request) {
		return $this->registration($request);
	}

	/**
	 * Update a client
	 *
	 * @ApiDoc(input={"class"="_Base\BackendBundle\Form\ClientType", "name"=""})
	 *
	 * @param Request $request
	 * @param integer $id      ID da categoria
	 *
	 * @return Response
	 */
	public function putAction(Request $request, $id) {
		return $this->registration($request, $id);
	}

	/**
	 * Remove a client
	 *
	 * @ApiDoc()
	 *
	 * @param integer $id ID da categoria
	 *
	 * @return Response
	 */
	public function deleteAction($id) {
		$repository = $this->getRepository(Client::class);
		return $this->createReponseForDeleteOne($repository, $id);
	}

	/**
	 *
	 * @param Request $request
	 * @param null    $id
	 *
	 * @return Response
	 */
	private function registration(Request $request, $id = null) {
		$isPost = $request->isMethod(Request::METHOD_POST);

		$sentData = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();

		$object = $isPost
			? new Client()
			: $em->getRepository(Client::class)->find($id);

		if (!$object) {
			return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
		}

		$form = $this->createForm(ClientType::class, $object, ($isPost ? [] : ['method' => Request::METHOD_PUT]));
		$form->submit($sentData, $isPost);

		if (!$form->isValid()) {
			return $this->handleView($this->view($form->getErrors(), Codes::HTTP_BAD_REQUEST));
		}

		$em->persist($object);
		$em->flush();

		return $this->handleView($this->view($object, Codes::HTTP_CREATED));
	}

}
