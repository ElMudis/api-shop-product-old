<?php





namespace _Base\BackendBundle\Controller\Entidades;

use _Base\BackendBundle\Entity\ItemCart;
use _Base\BackendBundle\Form\ItemCartType;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use Hateoas\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Response;
use _Base\ApiBundle\Controller\ApiController;


class ItemCartController extends ApiController
{
	/**
	 * Listagem dos registros
	 *
	 * @ApiDoc(resource=true)
	 *
	 * @QueryParam(name="page", description="Número da página. Padrão 1")
	 * @QueryParam(name="limit", description="Número de registros por página. Padrão 50")
	 * @QueryParam(name="nome", description="Filtre pelo nome de alguma itemcart")
	 *
	 * @param ParamFetcherInterface $paramFetcher
	 *
	 * @return Response
	 */
	public function cgetAction( ParamFetcherInterface $paramFetcher) {
		$queryBuilder = $this
		->getRepository(ItemCart::class)
		->findByParamFetcher($paramFetcher, 'itemcart');

		return $this->createResponseForCollection($paramFetcher, $queryBuilder, new Route('get_itemcarts'));
	}

	/**
	 * Detalhes de um registro
	 *
	 * @ApiDoc
	 *
	 * @param integer $id ID do registro
	 * @return Response
	 */
	public function getAction($id) {
		return $this->createResponseForFindOne(ItemCart::class, $id);
	}

	/**
	 * Insersão de um registro
	 *
	 * @ApiDoc(input={"class"="_Base\BackendBundle\Form\ItemCartType", "name"=""})
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function postAction(Request $request) {
		return $this->registration($request);
	}

	/**
	 * Atualização de um registro
	 *
	 * @ApiDoc(input={"class"="_Base\BackendBundle\Form\ItemCartType", "name"=""})
	 *
	 * @param Request $request
	 * @param integer $id      ID da categoria
	 *
	 * @return Response
	 */
	public function putAction(Request $request, $id) {
		return $this->registration($request, $id);
	}

	/**
	 * Exclusão de um registro
	 *
	 * @ApiDoc()
	 *
	 * @param integer $id ID da categoria
	 *
	 * @return Response
	 */
	public function deleteAction($id) {
		$repository = $this->getRepository(ItemCart::class);
		return $this->createReponseForDeleteOne($repository, $id);
	}

	/**
	 * Cadastra ou atualiza o registro conforme a definição do METHOD.
	 *
	 * @param Request $request
	 * @param null    $id
	 *
	 * @return Response
	 */
	private function registration(Request $request, $id = null) {
		$isPost = $request->isMethod(Request::METHOD_POST);

		$sentData = json_decode($request->getContent(), true);
		$em = $this->getDoctrine()->getManager();

		$object = $isPost
			? new ItemCart()
			: $em->getRepository(ItemCart::class)->find($id);

		if (!$object) {
			return $this->handleView($this->view(null, Codes::HTTP_NOT_FOUND));
		}

		$form = $this->createForm(ItemCartType::class, $object, ($isPost ? [] : ['method' => Request::METHOD_PUT]));
		$form->submit($sentData, $isPost);

		if (!$form->isValid()) {
			return $this->handleView($this->view($form->getErrors(), Codes::HTTP_BAD_REQUEST));
		}

		$em->persist($object);
		$em->flush();

		return $this->handleView($this->view($object, Codes::HTTP_CREATED));
	}

}
