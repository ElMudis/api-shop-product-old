<?php

namespace _Base\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CartRepository extends EntityRepository
{
	use ParamFetcherRepositoryTrait;

	protected function getFilters( $alias, $queryBuilder) {
		return [

		'totalPrice' => function($value) use ($alias, $queryBuilder) {
			return $queryBuilder
			->andWhere($alias . '.totalPrice LIKE :totalPrice')
			->setParameter(':totalPrice', sprintf('%%%s%%', $value));
		},




		];
	}
}
