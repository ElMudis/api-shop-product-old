<?php

namespace _Base\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ClientRepository extends EntityRepository
{
	use ParamFetcherRepositoryTrait;

	protected function getFilters( $alias, $queryBuilder) {
		return [

		'name' => function($value) use ($alias, $queryBuilder) {
			return $queryBuilder
			->andWhere($alias . '.name LIKE :name')
			->setParameter(':name', sprintf('%%%s%%', $value));
		},
		'email' => function($value) use ($alias, $queryBuilder) {
			return $queryBuilder
			->andWhere($alias . '.email LIKE :email')
			->setParameter(':email', sprintf('%%%s%%', $value));
		},



		];
	}
}
